import { Button, Grid, GridRow } from "semantic-ui-react";
import _ from "lodash";

const AnswerButton = ({
  buttonIdx,
  buttonText,
  handleClick,
  markCorrect,
  markWrong,
}) => (
  <div
    className={
      `question__answer-button ` +
      `${markWrong && "question__answer-button__wrong"} ` +
      `${markCorrect && "question__answer-button__correct"}`
    }
    onClick={() => handleClick()}
  >
    <h1>{buttonIdx + 1}</h1> {buttonText}
  </div>
);

const isAnswerMarkedCorrect = (idx, question, selectedAnswerIdx) =>
  selectedAnswerIdx !== null && idx === question.correct_answer;

const isAnswerMarkedWrong = (idx, question, selectedAnswerIdx) =>
  idx === selectedAnswerIdx && idx !== question.correct_answer;

const Question = ({
  question,
  selectedAnswerIdx,
  handleSelectAnswer,
  handleNext,
}) => (
  <>
    <Grid relaxed centered stackable columns={2}>
      {question.image && (
        <GridRow columns={1}>
          <Grid.Column textAlign="center">
            <img src={question.image} alt="" />
          </Grid.Column>
        </GridRow>
      )}

      <GridRow columns={1}>
        <Grid.Column>
          <h1>
            {question.id}. {question.question}
          </h1>
        </Grid.Column>
      </GridRow>

      <GridRow>
        {_.map(question.answers, (data, idx) => (
          <Grid.Column key={`question_select_answer_${idx}`}>
            <AnswerButton
              buttonIdx={idx}
              buttonText={data}
              handleClick={() => handleSelectAnswer(idx)}
              markCorrect={isAnswerMarkedCorrect(
                idx,
                question,
                selectedAnswerIdx
              )}
              markWrong={isAnswerMarkedWrong(idx, question, selectedAnswerIdx)}
            />
          </Grid.Column>
        ))}
      </GridRow>
    </Grid>

    <br />

    <Button
      disabled={selectedAnswerIdx === null}
      onClick={handleNext}
      size="big"
    >
      Следующий вопрос
    </Button>
    <br />
  </>
);

export default Question;
