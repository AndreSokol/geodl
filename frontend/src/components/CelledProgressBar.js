import _ from "lodash";
import { Progress } from "semantic-ui-react";

import "./CelledProgressBar.css";

const Cell = ({ isAnswered, isCorrect }) => {
  // const className = `progress_bar__cell ${
  //   isAnswered &&
  //   (isCorrect ? "progress_bar__cell__correct" : "progress_bar__cell__correct")
  // }`;
  const className = `progress_bar__cell ${
    isAnswered &&
    (isCorrect ? "progress_bar__cell__correct" : "progress_bar__cell__correct")
  }`;

  return <div className={className}>&nbsp;</div>;
};

const CelledProgressBar = ({ cells }) => (
  <>
    <Progress />
    <div className="progress_bar__container">
      {_.map(cells, (cell) => (
        <Cell isAnswered={cell.isAnswered} isCorrect={cell.isCorrect} />
      ))}
    </div>
  </>
);

export default CelledProgressBar;
