/* eslint-disable */

import { Component } from "react";
import { connect } from "react-redux";
import { Button, Form, Grid } from "semantic-ui-react";

class TelegramLoginButton extends Component {
  componentDidMount() {
    const {
      botName,
      buttonSize,
      cornerRadius,
      requestAccess,
      usePic,
      dataOnauth,
      dataAuthUrl,
      lang,
      widgetVersion,
    } = this.props;
    window.TelegramLoginWidget = {
      dataOnauth: (user) => dataOnauth(user),
    };

    const script = document.createElement("script");
    script.src = "https://telegram.org/js/telegram-widget.js?" + widgetVersion;
    script.setAttribute("data-telegram-login", botName);
    script.setAttribute("data-size", buttonSize);
    if (cornerRadius !== undefined) {
      script.setAttribute("data-radius", cornerRadius);
    }
    script.setAttribute("data-request-access", requestAccess);
    script.setAttribute("data-userpic", usePic);
    script.setAttribute("data-lang", lang);
    if (dataAuthUrl !== undefined) {
      script.setAttribute("data-auth-url", dataAuthUrl);
    } else {
      script.setAttribute(
        "data-onauth",
        "TelegramLoginWidget.dataOnauth(user)"
      );
    }
    script.async = true;

    if (process.env.NODE_ENV !== "development") {
      this.instance.appendChild(script);
    }
  }

  state = { username: "" };

  render() {
    if (this.props.isLoggedIn) {
      return null;
    }

    if (process.env.NODE_ENV === "development") {
      return (
        <Grid
          textAlign="center"
          style={{ height: "100vh" }}
          verticalAlign="middle"
        >
          <Grid.Column
            style={{ maxWidth: 450 }}
            onSubmit={() =>
              this.props.dataOnauth({
                username: this.state.username,
              })
            }
          >
            <Form>
              <Form.Input
                placeholder="Telegram username"
                size="large"
                value={this.state.username}
                onChange={(e, { name, value }) =>
                  this.setState({ username: value })
                }
              />
              <Button type="submit" size="large">
                Войти
              </Button>
            </Form>
          </Grid.Column>
        </Grid>
      );
    }

    return (
      <Grid
        textAlign="center"
        style={{ height: "100vh" }}
        verticalAlign="middle"
      >
        <Grid.Column style={{ maxWidth: 450 }}>
          <div
            className={this.props.className}
            ref={(component) => {
              this.instance = component;
            }}
          >
            {this.props.children}
          </div>
        </Grid.Column>
      </Grid>
    );
  }
}

TelegramLoginButton.defaultProps = {
  botName: "samplebot",
  dataOnauth: () => undefined,
  buttonSize: "large",
  requestAccess: "write",
  usePic: true,
  lang: "en",
  widgetVersion: 9,
};

const mapStateToProps = (state) => ({ isLoggedIn: state.userInfo.isLoggedIn });

export default connect(mapStateToProps)(TelegramLoginButton);
