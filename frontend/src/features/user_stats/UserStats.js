import { Component, Fragment } from "react";
import { connect } from "react-redux";
import _ from "lodash";
import { Button, Container, Divider, Loader, Statistic, Table } from "semantic-ui-react";
import { withRouter } from "react-router";
import { Link } from "react-router-dom";

import store, { fetchUserStats, setSolutionMode } from "../../app/store";

class UserStats extends Component {
  componentDidMount() {
    store.dispatch(fetchUserStats);
  }

  handleSelectMode(mode) {
    store.dispatch(setSolutionMode(mode));

    // eslint-disable-next-line react/destructuring-assignment
    this.props.history.push("/question");
  }

  renderStats() {
    const { userInfo } = this.props;

    if (!userInfo.stats) {
      return <Loader active inline />;
    }

    return (
      <>
        <Statistic.Group widths="three" horizontal>
          <Statistic>
            <Statistic.Value>{userInfo.stats.totalAnswers}</Statistic.Value>
            <Statistic.Label>ответов</Statistic.Label>
          </Statistic>

          <Statistic>
            <Statistic.Value>{userInfo.stats.questionsTried}</Statistic.Value>
            <Statistic.Label>отработанных вопросов</Statistic.Label>
          </Statistic>

          <Statistic>
            <Statistic.Value>
              {_.floor(userInfo.stats.correctRatio * 100, 2)}%
            </Statistic.Value>
            <Statistic.Label>правильных ответов</Statistic.Label>
          </Statistic>
        </Statistic.Group>

        <br />

        <Table celled>
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell>Номер билета</Table.HeaderCell>
              <Table.HeaderCell>Прорешано</Table.HeaderCell>
              <Table.HeaderCell>Попыток</Table.HeaderCell>
              <Table.HeaderCell>Доля корректных ответов</Table.HeaderCell>
            </Table.Row>
          </Table.Header>
          <Table.Body>
            {_.map(userInfo.stats.byTopic, (entry) => (
              <Table.Row key={`userstats_topics_${entry.topic_id}`}>
                <Table.Cell>
                  <Link to={`/topic/${entry.topic_id}`}>
                    {entry.topic_id}. {entry.topic_name}
                  </Link>
                </Table.Cell>
                <Table.Cell>
                  {entry.questions_opened} / {entry.questions_in_topic}
                  &nbsp;&nbsp;&nbsp;(
                  {_.floor(
                    (entry.questions_opened / entry.questions_in_topic) * 100
                  )}
                  %)
                </Table.Cell>
                <Table.Cell>{entry.total_retries}</Table.Cell>
                <Table.Cell>
                  {_.floor(entry.success_ratio * 100, 2)}%
                </Table.Cell>
              </Table.Row>
            ))}
          </Table.Body>
        </Table>
      </>
    );
  }

  render() {
    const { userInfo } = this.props;

    return (
      <Container>
        <br />
        <h1>Привет, {userInfo.user.first_name}!</h1>
        <br />
        <div>
          <h3>Что будем делать сегодня?</h3>
          {userInfo.stats && userInfo.stats.nextSuggestedTopicId && (
            <Link
              component={Button}
              to={`/topic/${userInfo.stats.nextSuggestedTopicId}`}
            >
              Новая тема
            </Link>
          )}
          <Button onClick={() => this.handleSelectMode("fix_mistakes")}>
            Работа над ошибками
          </Button>
          <Button onClick={() => this.handleSelectMode("repeat")} disabled>
            Повторение пройденного
          </Button>
        </div>
        <br />

        <Divider horizontal>
          <h4>Статистика</h4>
        </Divider>

        {this.renderStats()}
      </Container>
    );
  }
}

const mapStateToProps = (state) => ({ userInfo: state.userInfo });
export default connect(mapStateToProps)(withRouter(UserStats));
