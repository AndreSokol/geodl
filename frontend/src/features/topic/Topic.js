import { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router";
import { Button, Container, Header, Loader } from "semantic-ui-react";
import _ from "lodash";
import { Link } from "react-router-dom";

import { fetchTopic, submitQuestionAnswer } from "../../app/store";
import TopicQuestion from "../../components/Question";

class Topic extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: true,
    };
  }

  componentDidMount() {
    this.fetchTopicAndReset();
  }

  // eslint-disable-next-line no-unused-vars
  componentDidUpdate(prevProps, prevState, snapshot) {
    const {
      match: {
        params: { topicId },
      },
    } = this.props;

    const {
      match: {
        params: { topicId: prevTopicId },
      },
    } = prevProps;

    if (topicId !== prevTopicId) {
      this.fetchTopicAndReset();
    }
  }

  // eslint-disable-next-line react/sort-comp
  fetchTopicAndReset() {
    const {
      match: {
        params: { topicId },
      },
    } = this.props;

    this.setState({ isLoading: true });

    fetchTopic(topicId).then((res) =>
      this.setState({
        isLoading: false,
        topic: res.topic,
        questions: res.questions,
        answers: _.times(res.questions.length, () => null),
        currentQuestionIdx: null,
      })
    );
  }

  handleSelectAnswer(selectedAnswerIdx) {
    const { answers: newAnswers, questions, currentQuestionIdx } = this.state;
    newAnswers[currentQuestionIdx] = selectedAnswerIdx;
    this.setState({ answers: newAnswers });

    submitQuestionAnswer(
      questions[currentQuestionIdx].id,
      questions[currentQuestionIdx].correct_answer === selectedAnswerIdx
    );
  }

  renderDescription() {
    const { topic, questions } = this.state;

    return (
      <>
        <Header as="h1">
          {topic.name}
          <Header.Subheader>{questions.length} вопроса</Header.Subheader>
        </Header>

        <p>Здесь появится подготовительный текст</p>

        <Button
          positive
          onClick={() => this.setState({ currentQuestionIdx: 0 })}
        >
          К вопросам
        </Button>
      </>
    );
  }

  renderFinish() {
    const { questions, answers } = this.state;

    let correctAnswersCount = 0;
    for (let i = 0; i < questions.length; i += 1) {
      if (questions[i].correct_answer === answers[i]) {
        correctAnswersCount += 1;
      }
    }

    return (
      <>
        <Header>Ура!</Header>
        <p>
          Правильных ответов: {correctAnswersCount} из {questions.length}
        </p>
        <Button
          content="К списку тем"
          icon="left arrow"
          labelPosition="left"
          as={Link}
          to="/stats"
        />
        <Button
          content="Повторить эту тему"
          icon="repeat"
          labelPosition="left"
          onClick={() =>
            this.setState({
              answers: _.times(questions.length, () => null),
              currentQuestionIdx: null,
            })
          }
        />
        <Button
          positive
          content="К следующей теме"
          icon="shipping fast"
          labelPosition="right"
          as={Link}
          /* FIXME */
          /* eslint-disable-next-line react/destructuring-assignment */
          to={`/topic/${+this.props.match.params.topicId + 1}`}
        />
      </>
    );
  }

  renderContent() {
    const { questions, answers, currentQuestionIdx } = this.state;

    if (currentQuestionIdx === null) {
      return this.renderDescription();
    }

    if (currentQuestionIdx >= questions.length) {
      return this.renderFinish();
    }

    return (
      <TopicQuestion
        question={questions[currentQuestionIdx]}
        selectedAnswerIdx={answers[currentQuestionIdx]}
        handleSelectAnswer={(selectedAnswerIdx) =>
          this.handleSelectAnswer(selectedAnswerIdx)
        }
        handleNext={() =>
          this.setState({ currentQuestionIdx: currentQuestionIdx + 1 })
        }
      />
    );
  }

  render() {
    const { isLoading, questions, answers, currentQuestionIdx } = this.state;

    if (isLoading) {
      return (
        <div>
          <Loader inline size="huge" />
        </div>
      );
    }

    return (
      <Container>
        <Button.Group fluid>
          {_.map(_.range(questions.length), (i) => (
            <Button
              positive={
                answers[i] !== null &&
                answers[i] === questions[i].correct_answer
              }
              negative={
                answers[i] !== null &&
                answers[i] !== questions[i].correct_answer
              }
              active={i === currentQuestionIdx}
              style={{
                paddingLeft: 0,
                paddingRight: 0,
                borderRight: "1px solid white",
              }}
              key={`topic_progress_bar_${i}`}
              onClick={() => this.setState({ currentQuestionIdx: i })}
            />
          ))}
        </Button.Group>

        {this.renderContent()}
      </Container>
    );
  }
}

const mapStateToProps = (state) => ({ x: state.authData });

export default connect(mapStateToProps)(withRouter(Topic));
