import { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { Button, Container, Dropdown, Grid, GridRow } from "semantic-ui-react";
import _ from "lodash";

import store, {
  changeSolutionModeAndNextQuestion,
  nextQuestion,
  submitQuestionAnswer,
} from "../../app/store";

const options = [
  { key: 1, text: "новые билеты", value: "new" },
  { key: 2, text: "работа над ошибками", value: "fix_mistakes" },
  { key: 3, text: "повторение", value: "remind" },
];

const AnswerButton = ({
  buttonIdx,
  buttonText,
  handleClick,
  markCorrect,
  markWrong,
}) => (
  <div
    className={
      `question__answer-button ` +
      `${markWrong && "question__answer-button__wrong"} ` +
      `${markCorrect && "question__answer-button__correct"}`
    }
    onClick={() => handleClick()}
  >
    <h1>{buttonIdx + 1}</h1> {buttonText}
  </div>
);

class Question extends Component {
  constructor(props) {
    super(props);

    this.state = {
      selectedIdx: null,
      question: null,
      isLoading: true,
    };
  }

  componentDidMount() {
    const {
      solutionMode: { mode, params },
    } = this.props;
    nextQuestion(mode, params).then(({ question }) =>
      this.setState({ question, isLoading: false, selectedIdx: null })
    );
  }

  handleSelectAnswer(idx) {
    this.setState({ selectedIdx: idx });

    const { question } = this.state;

    window.setTimeout(
      () =>
        submitQuestionAnswer(question.id, question.correct_answer === idx).then(
          ({ question: newQuestion }) =>
            this.setState({
              question: newQuestion,
              isLoading: false,
              selectedIdx: null,
            })
        ),
      1000
    );
  }

  handleChangeMode(newMode) {
    this.setState({ isLoading: true });
    store.dispatch(
      changeSolutionModeAndNextQuestion(newMode, ({ question: newQuestion }) =>
        this.setState({
          question: newQuestion,
          isLoading: false,
          selectedIdx: null,
        })
      )
    );
  }

  isAnswerMarkedCorrect(idx) {
    const { selectedIdx, question } = this.state;
    return selectedIdx !== null && idx === question.correct_answer;
  }

  isAnswerMarkedWrong(idx) {
    const { selectedIdx, question } = this.state;
    return idx === selectedIdx && idx !== question.correct_answer;
  }

  render() {
    const { solutionMode } = this.props;
    const { question, isLoading } = this.state;

    if (isLoading) {
      return <div>Loading</div>;
    }

    return (
      <Container>
        <Grid columns="equal" verticalAlign="middle" style={{ marginTop: 0 }}>
          <Grid.Column>
            <Link to="/stats">
              <Button content="назад" icon="left arrow" labelPosition="left" />
            </Link>
          </Grid.Column>
          <Grid.Column textAlign="right">
            Режим:&nbsp;
            <Dropdown
              onChange={(event, value) => this.handleChangeMode(value.value)}
              options={options}
              placeholder="Choose an option"
              value={solutionMode.mode}
            />
          </Grid.Column>
        </Grid>

        <Grid relaxed centered stackable columns={2}>
          <GridRow columns={1}>
            <Grid.Column textAlign="center">
              <img src={question.image} alt="" />
            </Grid.Column>
          </GridRow>

          <GridRow columns={1}>
            <Grid.Column>
              <h1>
                {question.id}. {question.question}
              </h1>
            </Grid.Column>
          </GridRow>

          <GridRow>
            {_.map(question.answers, (data, idx) => (
              <Grid.Column>
                <AnswerButton
                  buttonIdx={idx}
                  buttonText={data}
                  handleClick={() => this.handleSelectAnswer(idx)}
                  markCorrect={this.isAnswerMarkedCorrect(idx)}
                  markWrong={this.isAnswerMarkedWrong(idx)}
                />
              </Grid.Column>
            ))}
          </GridRow>
        </Grid>
      </Container>
    );
  }
}

const mapStateToProps = (state) => ({ solutionMode: state.solutionMode });

export default connect(mapStateToProps)(Question);
