import {
  Container,
  Divider,
  Flag,
  Grid,
  Header,
  Icon,
  Image,
  Label,
  Menu,
  Segment,
} from "semantic-ui-react";
import { Link } from "react-router-dom";

import { isMobile } from "../../utils";

const ColoredHeader = ({ children, color }) => (
  <Header color={color} as="h4" style={{ margin: "0.5rem 0" }}>
    {children}
  </Header>
);

const Landing = () => (
  <>
    <Menu borderless>
      <Container>
        <Menu.Item>
          <Header
            size={isMobile() ? "big" : "huge"}
            style={{ padding: "1.5rem 0" }}
          >
            <Icon name="shipping fast" />
            <Header.Content>Грузинские движения</Header.Content>
          </Header>
        </Menu.Item>
        <Menu.Item position="right" as={Link} to="/stats">
          Войти
        </Menu.Item>
      </Container>
    </Menu>

    {/*<Container>*/}
    {/*  <Divider hidden section />*/}

    {/*  <Grid columns={2}>*/}
    {/*    <Grid.Row>*/}
    {/*      <Grid.Column>*/}
    {/*        <Header as="h2">*/}
    {/*          Получить права в России <Flag name="ru" />*/}
    {/*        </Header>*/}
    {/*      </Grid.Column>*/}
    {/*      <Grid.Column>*/}
    {/*        <Header as="h2">*/}
    {/*          Получить права в Грузии <Flag name="ge" />*/}
    {/*        </Header>*/}
    {/*      </Grid.Column>*/}
    {/*    </Grid.Row>*/}

    {/*    <Grid.Row>*/}
    {/*      <Grid.Column>*/}
    {/*        <ColoredHeader color="red" as="h4">*/}
    {/*          ~30 000 RUB за автошколу*/}
    {/*        </ColoredHeader>*/}
    {/*        <ColoredHeader color="red" as="h4">*/}
    {/*          Срок получения от 6 месяцев до года*/}
    {/*        </ColoredHeader>*/}
    {/*        <ColoredHeader color="red" as="h4">*/}
    {/*          Несколько дней, потраченных в обшарпанных отделениях ГИБДД*/}
    {/*        </ColoredHeader>*/}
    {/*        <ColoredHeader color="red" as="h4">*/}
    {/*          Вымогают взятки*/}
    {/*        </ColoredHeader>*/}
    {/*      </Grid.Column>*/}
    {/*      <Grid.Column>*/}
    {/*        <ColoredHeader color="green" as="h4">*/}
    {/*          8000 рублей, включая занятия, справки и госпошлины*/}
    {/*        </ColoredHeader>*/}
    {/*        <ColoredHeader color="green" as="h4">*/}
    {/*          6 недель с нуля*/}
    {/*        </ColoredHeader>*/}
    {/*        <ColoredHeader color="green" as="h4">*/}
    {/*          Один двухчасовой визит в современный центр госуслуг*/}
    {/*        </ColoredHeader>*/}
    {/*        <ColoredHeader color="green" as="h4">*/}
    {/*          Нет взяток, потому что все автоматизировано - даже практика!*/}
    {/*        </ColoredHeader>*/}
    {/*      </Grid.Column>*/}
    {/*    </Grid.Row>*/}
    {/*  </Grid>*/}
    {/*</Container>*/}

    {/*<Divider hidden section />*/}

    {/*<Container text>*/}
    {/*  <Header as="h2">Где будут действительны грузинские права?</Header>*/}

    {/*  <p>*/}
    {/*    Права, выданные в Грузии, действуют во всех странах, подписавших Венскую*/}
    {/*    конвенцию - включая Россию. Дополнительно за отдельную пошлину можно*/}
    {/*    оформить международное водительское удостоверение - его принимают почти*/}
    {/*    по всему миру.*/}
    {/*  </p>*/}
    {/*</Container>*/}

    {/*<Divider hidden />*/}

    {/*<Grid stackable relaxed="very" padded centered>*/}
    {/*  <Grid.Column textAlign="center" width={4}>*/}
    {/*    <a href="https://ru.wikipedia.org/wiki/Венская_конвенция_о_дорожном_движении">*/}
    {/*      <Label>Страны-участницы Венской конвенции</Label>*/}
    {/*      <Image*/}
    {/*        centered*/}
    {/*        src="https://upload.wikimedia.org/wikipedia/commons/thumb/c/c3/Vienna_Convention_on_Road_Traffic.svg/2880px-Vienna_Convention_on_Road_Traffic.svg.png"*/}
    {/*      />*/}
    {/*    </a>*/}
    {/*  </Grid.Column>*/}
    {/*  <Grid.Column textAlign="center" width={4}>*/}
    {/*    <a href="https://ru.wikipedia.org/wiki/Международное_водительское_удостоверение">*/}
    {/*      <Label>Страны, где действительно международное ВУ</Label>*/}
    {/*      <Image*/}
    {/*        centered*/}
    {/*        src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/92/Geneva_Convention_on_Road_Traffic.svg/2880px-Geneva_Convention_on_Road_Traffic.svg.png"*/}
    {/*      />*/}
    {/*    </a>*/}
    {/*  </Grid.Column>*/}
    {/*</Grid>*/}

    {/*<Divider hidden section />*/}

    {/*<Container text>*/}
    {/*  <Header as="h2">Можно ли по грузинским правам ездить в России?</Header>*/}

    {/*  <p>Можно! Согласно закону о дорожном движении:</p>*/}

    {/*  <Segment>*/}
    {/*    <Header as="h4">*/}
    {/*      Статья 25. Основные положения, касающиеся допуска к управлению*/}
    {/*      транспортными средствами*/}
    {/*    </Header>*/}
    {/*    <p style={{ textAlign: "center", padding: 0, margin: 0 }}>...</p>*/}
    {/*    <p>*/}
    {/*      12. Лица, постоянно или временно проживающие либо временно пребывающие*/}
    {/*      на территории Российской Федерации, допускаются к управлению*/}
    {/*      транспортными средствами на основании российских национальных*/}
    {/*      водительских удостоверений, а при отсутствии таковых - на основании*/}
    {/*      иностранных национальных или международных водительских удостоверений*/}
    {/*      при соблюдении ограничений, указанных в пункте 13 настоящей статьи.*/}
    {/*    </p>*/}
    {/*    <p style={{ textAlign: "center", padding: 0, margin: 0 }}>...</p>*/}

    {/*    <a href="http://www.consultant.ru/document/cons_doc_LAW_8585/1e24735df982c4fb8bf865fe29270095749ba3bc/">*/}
    {/*      <p style={{ textAlign: "right" }}>*/}
    {/*        -- Федеральный закон от 10.12.1995 N 196-ФЗ (ред. от 02.07.2021)*/}
    {/*        <br />*/}
    {/*        /!* eslint-disable-next-line react/no-unescaped-entities *!/*/}
    {/*        "О безопасности дорожного движения"*/}
    {/*        /!* FIXME *!/*/}
    {/*      </p>*/}
    {/*    </a>*/}
    {/*  </Segment>*/}

    {/*  <p>Несколько неудачную формулировку уточняет МВД:</p>*/}

    {/*  <Segment>*/}
    {/*    <p>*/}
    {/*      В настоящее время все лица (*/}
    {/*      <b>*/}
    {/*        как граждане Российской Федерации, так и иностранные граждане и лица*/}
    {/*        без гражданства*/}
    {/*      </b>*/}
    {/*      ), постоянно или временно проживающие либо временно пребывающие на*/}
    {/*      территории Российской Федерации, допускаются к управлению*/}
    {/*      транспортными средствами на основании российских национальных*/}
    {/*      водительских удостоверений, а при отсутствии таковых - на основании*/}
    {/*      иностранных национальных или международных водительских удостоверений.*/}
    {/*    </p>*/}

    {/*    <a href="https://67.мвд.рф/document/3415210">*/}
    {/*      <p style={{ textAlign: "right" }}>*/}
    {/*        -- Управление МВД России по Смоленской области*/}
    {/*      </p>*/}
    {/*    </a>*/}
    {/*  </Segment>*/}

    {/*  <p>*/}
    {/*    Однако, учитывая специфику работы ГИБДД в России, для спокойной жизни мы*/}
    {/*    рекомендуем гражданам РФ со временем поменять права на российские. Для*/}
    {/*    этого надо будет сдать экзамен в России, но уже без автошколы, заплатив*/}
    {/*    только госпошлину в 2000 рублей.*/}
    {/*  </p>*/}

    {/*  <Divider hidden section />*/}
    {/*</Container>*/}
  </>
);
export default Landing;
