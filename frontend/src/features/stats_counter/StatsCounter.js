import { connect } from "react-redux";

const StatsCounter = ({ stats }) => {
  const { totalAnswered, correctRatio } = stats;

  return (
    <div
      style={{
        position: "absolute",
        right: 0,
        padding: "15px",
        backgroundColor: "#eee",
        borderRadius: "0 0 0 15px",
      }}
    >
      <div>Всего отвечено: {totalAnswered}</div>
      <div>Доля корректных: {correctRatio}%</div>
    </div>
  );
};

const mapStateToProps = (state) => ({ stats: state.stats });

export default connect(mapStateToProps)(StatsCounter);
