import React from "react";
import { connect } from "react-redux";
import { Route, Router, Switch } from "react-router-dom";
import { createBrowserHistory } from "history";

import "./App.css";

import Question from "./features/question/Question";
import TelegramLoginButton from "./features/telegram_login_button/TelegramLoginButton";
import store, { login } from "./app/store";
import UserStats from "./features/user_stats/UserStats";
import Topic from "./features/topic/Topic";
import Landing from "./features/landing/Landing";

const history = createBrowserHistory();

function App({ isLoggedIn }) {
  return (
    <Router history={history}>
      <Switch>
        <Route path="/" exact>
          <Landing />
        </Route>

        {!isLoggedIn ? (
          <TelegramLoginButton
            botName="andresokolbot"
            dataOnauth={(authData) => store.dispatch(login(authData))}
          />
        ) : (
          <>
            <Route path="/question">
              <Question />
            </Route>

            <Route path="/topic/:topicId">
              <Topic />
            </Route>

            <Route path="/stats">
              <UserStats />
            </Route>
          </>
        )}
      </Switch>
    </Router>
  );
}

const mapStateToProps = (state) => ({ isLoggedIn: state.userInfo.isLoggedIn });
export default connect(mapStateToProps)(App);
