/* eslint no-unused-vars: 0 */

import { applyMiddleware, combineReducers, createStore } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import thunkMiddleware from "redux-thunk";

const initialState = {
  stats: {
    totalAnswered: 0,
    correctRatio: "0.0",
  },
  solutionMode: {
    mode: "new",
    params: null,
  },
  userInfo: {
    isLoggedIn: false,
    user: null,
    stats: null,
  },
};

const statsReducer = (state = initialState.stats, action) => {
  switch (action.type) {
    case "question/questionLoaded":
      return {
        totalAnswered: action.payload.stats.total_answered,
        correctRatio: action.payload.stats.correct_ratio,
      };
    default:
      return state;
  }
};

const userInfoReducer = (state = initialState.userInfo, action) => {
  switch (action.type) {
    case "login/login":
      return {
        isLoggedIn: true,
        user: action.payload,
        stats: null,
      };
    case "login/logout":
      return initialState.userInfo;
    case "userInfo/userStatsLoaded":
      return {
        ...state,
        stats: {
          totalAnswers: action.payload.total_answers,
          questionsTried: action.payload.questions_tried,
          correctRatio: action.payload.correct_ratio,
          byTopic: action.payload.by_topic,
          nextSuggestedTopicId: action.payload.next_suggested_topic_id,
        },
      };
    default:
      return state;
  }
};

const solutionModeReducer = (state = initialState.solutionMode, action) => {
  switch (action.type) {
    case "solutionMode/set":
      return {
        mode: action.payload.mode,
        params: action.payload.params,
      };
    default:
      return state;
  }
};

const rootReducer = (state, action) =>
  combineReducers({
    stats: statsReducer,
    solutionMode: solutionModeReducer,
    userInfo: userInfoReducer,
  })(state, action);

export function login(authData) {
  return async function loginThunk(dispatch, getState) {
    const response = await fetch("/api/login", {
      method: "POST",
      body: JSON.stringify(authData),
    });
    const data = await response.json();
    dispatch({ type: "login/login", payload: data });
  };
}

export const fetchUserStats = async (dispatch, getState) => {
  const response = await fetch("/api/user_stats");
  const data = await response.json();
  dispatch({ type: "userInfo/userStatsLoaded", payload: data });
};

export const checkToken = async (dispatch, getState) => {
  const response = await fetch("/api/user_info");
  const data = await response.json();
  dispatch({ type: "login/login", payload: data });
};

export const nextQuestion = async (mode) => {
  const response = await fetch("/api/next_question", {
    method: "POST",
    body: JSON.stringify({ mode }),
  });
  return response.json();
};

export const fetchTopic = async (topicId) => {
  const response = await fetch("/api/topic", {
    method: "POST",
    body: JSON.stringify({ topic_id: topicId }),
  });
  return response.json();
};

export const submitQuestionAnswer = async (questionId, isCorrect) => {
  const response = await fetch("/api/register_select", {
    method: "POST",
    body: JSON.stringify({ question_id: questionId, is_correct: isCorrect }),
  });
  return response.json();
};

export const setSolutionMode =
  (mode, params = null) =>
  async (dispatch, getState) => {
    dispatch({ type: "solutionMode/set", payload: { mode, params } });
  };

export const changeSolutionModeAndNextQuestion =
  (mode, callback) => async (dispatch, getState) => {
    dispatch({ type: "solutionMode/set", payload: { mode, params: null } });
    return nextQuestion(mode).then(callback);
  };

const composedEnhancer = composeWithDevTools(applyMiddleware(thunkMiddleware));

const store = createStore(rootReducer, composedEnhancer);
export default store;
