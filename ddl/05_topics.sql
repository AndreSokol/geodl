CREATE TABLE geodl_topics
(
    id          INT PRIMARY KEY,
    name        TEXT,

    created_at  TIMESTAMP NOT NULL DEFAULT NOW(),
    modified_at TIMESTAMP NOT NULL DEFAULT NOW()
);