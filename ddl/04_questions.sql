CREATE TABLE geodl_questions
(
    id             INT PRIMARY KEY,
    topic_id       INT       NOT NULL,
    question       TEXT      NOT NULL,
    answers        TEXT[]    NOT NULL,
    correct_answer INT       NOT NULL,
    image          TEXT,

    created_at     TIMESTAMP NOT NULL DEFAULT NOW(),
    modified_at    TIMESTAMP NOT NULL DEFAULT NOW()
);