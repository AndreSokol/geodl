CREATE TABLE geodl_user_timeslots_notifications
(
    user_id              TEXT PRIMARY KEY,
    notification_enabled BOOLEAN                  NOT NULL DEFAULT true,
    cities               text[]                   NOT NULL,
    last_checked_at      TIMESTAMP WITH TIME ZONE,
    last_notified_at     TIMESTAMP WITH TIME ZONE,
    last_sent_timeslot   TEXT,
    created_at           TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(),
    modified_at          TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()
);


ALTER TABLE geodl_user_timeslots_notifications
ADD COLUMN last_sent_timeslots jsonb NOT NULL DEFAULT '{}'::jsonb;
ALTER TABLE geodl_user_timeslots_notifications
DROP COLUMN last_sent_timeslot;