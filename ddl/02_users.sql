CREATE TABLE geodl_users
(
    id            TEXT PRIMARY KEY   DEFAULT md5(now()::timestamp || 'salt')::text,
    tg_user_id    INT       NOT NULL,
    tg_username   TEXT,
    tg_first_name TEXT,
    tg_last_name  TEXT,
    tg_image_url  TEXT,

    created_at    TIMESTAMP NOT NULL DEFAULT now(),
    updated_at    TIMESTAMP NOT NULL DEFAULT now()
);
