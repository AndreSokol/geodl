CREATE TABLE geodl_auth_tokens
(
    id         TEXT PRIMARY KEY,
    user_id    TEXT      NOT NULL,
    created_at TIMESTAMP NOT NULL,
    expire_at  TIMESTAMP NOT NULL
);
