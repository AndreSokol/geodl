CREATE TABLE geodl_available_timeslots
(
    fetched_ts TIMESTAMP WITH TIME ZONE PRIMARY KEY,
    timeslots  jsonb NOT NULL
);
