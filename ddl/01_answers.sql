CREATE TABLE geodl_answers
(
    id          TEXT PRIMARY KEY   DEFAULT md5(now()::timestamp || 'salt')::text,
    user_id     TEXT      NOT NULL,
    question_id INT       NOT NULL,
    is_correct  BOOLEAN   NOT NULL,
    created_at  TIMESTAMP NOT NULL DEFAULT NOW()
);
