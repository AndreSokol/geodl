import asyncio
import datetime as dt
import json

import asyncpg.pool

from backend import tg_bot


async def send_message(user: dict, timeslots: dict):
    message = 'Появились новые слоты!\n'

    for city in user['cities']:
        message += f'\n== {city} ==\n'

        if not timeslots[city]:
            message += f'Нет свободных слотов :(\n'

        for slot in timeslots[city]:
            message += f'{dt.datetime.fromisoformat(slot).strftime("%d %b %Y, %a: %H:%M")}\n'

    print(message)
    await tg_bot.send_message(message, user['tg_user_id'])


def should_notify_user(user: dict, timeslots: dict) -> (bool, dict):
    last_sent_timeslots: dict = json.loads(user['last_sent_timeslots'])
    new_last_sent_timeslots = {}
    should_notify_global: bool = False

    print(f"Considering notifying user {user['user_id']}")
    for city in user['cities']:
        last_city_sent_slot = last_sent_timeslots.get(city)
        max_city_slot = None if not timeslots[city] else max(timeslots[city])

        if max_city_slot is None:
            should_notify = False
        elif last_city_sent_slot is None:
            should_notify = True
            new_last_sent_timeslots[city] = max_city_slot
        else:
            should_notify = last_city_sent_slot < max_city_slot
            new_last_sent_timeslots[city] = max(max_city_slot, last_city_sent_slot)

        print(f"{city}: last sent {last_city_sent_slot},",
              f"max available {max_city_slot},",
              f"should notify {should_notify}")
        should_notify_global = should_notify_global or should_notify

    print(f"Total for user {user['user_id']}: {should_notify_global}, {new_last_sent_timeslots}")
    return should_notify_global, new_last_sent_timeslots


async def handle_user(user: dict, timeslots: dict, conn: asyncpg.Connection):
    should_notify, max_sent_timeslots = should_notify_user(user, timeslots)

    if not should_notify:
        await conn.execute(
            '''UPDATE geodl_user_timeslots_notifications SET last_checked_at = NOW() WHERE user_id = $1''',
            user['user_id'])
        return

    await send_message(user, timeslots)

    await conn.execute('''
        UPDATE geodl_user_timeslots_notifications
        SET last_notified_at    = now(),
            last_checked_at     = now(),
            modified_at         = now(),
            last_sent_timeslots = $1
        WHERE user_id = $2
    ''', json.dumps(max_sent_timeslots), user['user_id'])


async def job_iteration(pg_pool: asyncpg.pool.Pool):
    async with pg_pool.acquire() as conn:
        users = await conn.fetch('''
        SELECT notifications.user_id                             AS user_id,
               users.tg_user_id                                  AS tg_user_id,
               notifications.cities                              AS cities,
               notifications.last_notified_at AT TIME ZONE 'UTC' AS last_modified_at,
               notifications.last_sent_timeslots                 AS last_sent_timeslots
        
        FROM geodl_user_timeslots_notifications AS notifications
                 JOIN geodl_users AS users on notifications.user_id = users.id
        WHERE notification_enabled IS TRUE
          AND (last_checked_at IS NULL OR NOW() - last_checked_at > INTERVAL '1 hour')
          AND (last_notified_at IS NULL OR NOW() - last_notified_at > INTERVAL '1 hour')
        ''')

        timeslots = await conn.fetchval('''
            SELECT timeslots FROM geodl_available_timeslots
            ORDER BY fetched_ts DESC LIMIT 1
        ''')
        timeslots = json.loads(timeslots)

        for user in users:
            await handle_user(user, timeslots, conn)


async def timeslots_notify_job(app):
    while True:
        try:
            await job_iteration(app.pg_pool)
        except (KeyboardInterrupt, asyncio.CancelledError):
            return
        except Exception as exc:
            print('Exception in timeslots_notify:', exc)

        await asyncio.sleep(5 * 60)
