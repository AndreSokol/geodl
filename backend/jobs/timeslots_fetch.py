import asyncio
import datetime
import datetime as dt
import json

import aiohttp
import asyncpg

SLEEP_SECONDS = 0.2
JOB_ITERATION_TIMEDELTA = dt.timedelta(hours=1)


async def fetch_cities(client: aiohttp.ClientSession):
    res = await client.post("https://pay.ge/Provider/Balance",
                            json={"providerID": 1658, "incomeParam": "{\"0\":\"ENG\"}"},
                            )

    print(await res.text())
    data = await res.json()
    return {elem['param2']: elem['Identifier'] for elem in data['balanceParams']}


async def fetch_city_dates(city_id: str, client: aiohttp.ClientSession):
    await asyncio.sleep(SLEEP_SECONDS)

    res = await client.post("https://pay.ge/Provider/Balance",
                            data={"providerID": 1659, "incomeParam": f'{{"0":"{city_id}"}}'})

    data = await res.json()

    if 'error' in data:
        print('no slots for', city_id)
        print(data)
        return []

    return [elem['param2'] for elem in data['balanceParams']]


async def fetch_city_timeslots(city_id: str, date: str, client: aiohttp.ClientSession):
    await asyncio.sleep(SLEEP_SECONDS)

    res = await client.post("https://pay.ge/Provider/Balance",
                            data={"providerID": 1660, "incomeParam": f'{{"0":"{city_id}", "1": "{date}"}}'})

    data = await res.json()
    return [elem['param2'] for elem in data['balanceParams']]


def construct_iso_str(datestr: str, slot: str) -> str:
    month, day, year = map(int, datestr.split('/'))
    hours, minutes = map(int, slot.split(':'))

    return datetime.datetime(year, month, day, hours, minutes).isoformat()


async def fetch_all_timeslots() -> dict:
    # return {'Kutaisi': {}, 'Batumi': {}, 'Telavi': {}, 'Akhaltsikhe': {},
    #         'Zugdidi': {'1/26/2022': ['10:00', '11:00', '14:00', '15:00'],
    #                     '1/27/2022': ['10:00', '11:00', '14:00', '15:00'],
    #                     '1/28/2022': ['10:00', '11:00', '14:00', '15:00'], '1/29/2022': ['10:00', '11:00', '12:00'],
    #                     '1/31/2022': ['10:00', '11:00', '14:00', '15:00']}, 'Gori': {},
    #         'Poti': {'1/25/2022': ['10:00'], '1/26/2022': ['10:00', '11:00', '12:00', '14:00', '15:00'],
    #                  '1/27/2022': ['10:00', '11:00', '12:00', '14:00', '15:00'],
    #                  '1/28/2022': ['10:00', '11:00', '12:00', '14:00', '15:00'], '1/29/2022': ['10:00', '11:00'],
    #                  '1/31/2022': ['10:00', '11:00', '12:00', '14:00']},
    #         'Ozurgeti': {'1/24/2022': ['10:00'], '1/25/2022': ['10:00', '11:00'],
    #                      '1/26/2022': ['10:00', '11:00', '12:00', '12:45'],
    #                      '1/27/2022': ['10:00', '11:00', '12:00', '12:45'],
    #                      '1/28/2022': ['10:00', '11:00', '12:00', '12:45'], '1/29/2022': ['10:00', '11:00'],
    #                      '1/31/2022': ['10:00', '11:00', '12:00', '12:45']}, 'Sachkhere': {}, 'Ambrolauri': {},
    #         'Akhalkalaki': {}, 'Rustavi': {}}

    free_slots = {}

    async with aiohttp.ClientSession() as client:
        cities = await fetch_cities(client)

        for city_name, city_id in cities.items():
            free_slots[city_name] = []

            dates = await fetch_city_dates(city_id, client)

            for date in dates:
                timeslots = await fetch_city_timeslots(city_id, date, client)
                free_slots[city_name] += [construct_iso_str(date, slot) for slot in timeslots]

    print(free_slots)
    return free_slots


async def job_iteration(pgpool: asyncpg.pool.Pool):
    async with pgpool.acquire() as conn:
        conn: asyncpg.connection.Connection
        last_fetched_ts: dt.datetime = await conn.fetchval('''
            SELECT fetched_ts AT TIME ZONE 'UTC' FROM geodl_available_timeslots
            ORDER BY fetched_ts DESC LIMIT 1
        ''')

        delta_since_last_fetch = dt.datetime.utcnow() - last_fetched_ts

        if delta_since_last_fetch < JOB_ITERATION_TIMEDELTA:
            return

        print('Starting fetching timeslots...')
        timeslots = await fetch_all_timeslots()
        print('Timeslots...', timeslots)
        await conn.execute('''INSERT INTO geodl_available_timeslots VALUES (NOW(), $1)''', json.dumps(timeslots))


async def timeslots_fetch_job(app):
    while True:
        try:
            await job_iteration(app.pg_pool)
        except (KeyboardInterrupt, asyncio.CancelledError):
            return
        except Exception as exc:
            print('Exception:', exc)

        await asyncio.sleep(5 * 60)


if __name__ == '__main__':
    asyncio.run(fetch_all_timeslots())
