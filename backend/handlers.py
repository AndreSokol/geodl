import hashlib
import hmac
import os
import random
import uuid

from aiohttp import web
import asyncpg

DEV_ENV = bool(os.getenv('DEV_ENV'))
TG_BOT_KEY = os.getenv('TG_BOT_KEY')


def _with_auth(handler):
    async def wrapped(req, *args, **kwargs):
        auth_token = req.cookies.get('auth-token')
        if not auth_token:
            return web.HTTPForbidden()

        async with req.app.pg_pool.acquire() as conn:
            user_id = await conn.fetchval('SELECT user_id FROM geodl_auth_tokens WHERE id = $1', auth_token)
        if not user_id:
            return web.HTTPForbidden()

        return await handler(req, *args, **kwargs, user_id=user_id)

    return wrapped


async def _get_question(conn: asyncpg.connection.Connection, question_id: str) -> dict:
    question = await conn.fetchrow('''
        SELECT id,
               topic_id,
               question,
               answers,
               correct_answer - 1 AS correct_answer,
               image
        FROM geodl_questions
        WHERE id = $1;
    ''', question_id)  # FIXME: -1

    return dict(question)


async def select_next_question_new(req, user_id: str) -> dict:
    async with req.app.pg_pool.acquire() as conn:
        unsolved_questions = await conn.fetch("""
            SELECT
                   q.id AS question_id
            FROM geodl_questions AS q
            LEFT OUTER JOIN (
                SELECT question_id FROM geodl_answers
                WHERE user_id = $1
                GROUP BY question_id
                ) AS a
            ON q.id = a.question_id
            WHERE a.question_id IS NULL;
        """, user_id)

        next_question_id = random.choice(unsolved_questions)['question_id']
        return await _get_question(conn, next_question_id)


async def select_next_question_mistakes(req, user_id: str) -> dict:
    async with req.app.pg_pool.acquire() as conn:
        worst_five = await conn.fetch('''
            SELECT question_id,
                   AVG(is_correct::int) AS success_ratio,
                   MAX(created_at)      AS last_answered_at_ts
            FROM geodl_answers
            WHERE user_id = $1
            GROUP BY question_id
            ORDER BY success_ratio ASC
            LIMIT 5
        ''', user_id)

        # selecting the least recent of worst 5
        worst_five.sort(key=lambda x: x['last_answered_at_ts'])
        question_id = worst_five[0]['question_id']

        return await _get_question(conn, question_id)


async def select_next_question(req, user_id: str, mode: str) -> dict:
    if mode == 'new':
        return await select_next_question_new(req, user_id)

    if mode == 'fix_mistakes':
        return await select_next_question_mistakes(req, user_id)

    if mode == 'remind':
        pass

    raise web.HTTPBadRequest(text=f'bad mode "{mode}"')


@_with_auth
async def handle_next_question(req, user_id: str):
    body = await req.json()
    mode = body['mode']

    question = await select_next_question(req, user_id, mode)
    return web.json_response({
        "question": question,
        "stats": None,
    })


@_with_auth
async def fetch_topic(req, user_id: str):
    body = await req.json()
    topic_id = int(body['topic_id'])

    async with req.app.pg_pool.acquire() as conn:
        topic_info = await conn.fetchrow("SELECT id, name FROM geodl_topics;")
        questions = await conn.fetch(
            "SELECT id, question, answers, correct_answer - 1 as correct_answer, image FROM geodl_questions WHERE topic_id = $1",
            topic_id)  # FIXME

    return web.json_response({
        "topic": dict(topic_info),
        "questions": [dict(question) for question in questions],
    })


@_with_auth
async def record_response(req, user_id):
    body = await req.json()

    pool: asyncpg.pool.Pool = req.app.pg_pool

    async with pool.acquire() as conn:
        await conn.execute("INSERT INTO geodl_answers (user_id, question_id, is_correct) VALUES($1, $2, $3)",
                           user_id, body['question_id'], body['is_correct'])

    question = await select_next_question(req, user_id, 'new')  # FIXME
    return web.json_response({
        "question": question,
        # "stats": stats,
    })


@_with_auth
async def user_info(req, user_id):
    pool: asyncpg.pool.Pool = req.app.pg_pool

    async with pool.acquire() as conn:
        user = await conn.fetchrow(
            "SELECT id, tg_username AS username, tg_first_name AS first_name FROM geodl_users WHERE id = $1", user_id)

    return web.json_response({
        **user
    })


@_with_auth
async def user_stats(req, user_id):
    pool: asyncpg.pool.Pool = req.app.pg_pool

    async with pool.acquire() as conn:
        global_stats = await conn.fetchrow("""
            SELECT COUNT(*)                               as total_answers,
                   COUNT(DISTINCT question_id)            as questions_tried,
                   AVG(is_correct::int)::double precision as correct_ratio
            FROM geodl_answers
            
            WHERE user_id = $1;
        """, user_id)

        stats_by_topic = await conn.fetch("""
            SELECT topics.id                                  AS topic_id,
                   topics.name                                AS topic_name,
                   COUNT(DISTINCT stats.question_id)          AS questions_opened,
                   COUNT(*)                                   AS questions_in_topic,
                   AVG(stats.success_ratio)::double precision AS success_ratio,
                   SUM(stats.retries)::int                    AS total_retries,
                   MAX(stats.last_answered_at_ts)::text       AS last_answered_at_ts
            FROM (SELECT question_id,
                         AVG(is_correct::int) as success_ratio,
                         COUNT(*)             as retries,
                         MAX(created_at)      as last_answered_at_ts
                  FROM geodl_answers
            
                  WHERE user_id = $1
                  GROUP BY question_id) AS stats
                     RIGHT OUTER JOIN geodl_questions AS questions
                                      ON stats.question_id = questions.id
                     JOIN geodl_topics AS topics ON questions.topic_id = topics.id
            GROUP BY topics.id, topics.name
            ORDER BY topics.id;
        """, user_id)

        next_suggested_topic_id = await conn.fetchval("""
            SELECT questions.topic_id
            FROM geodl_questions AS questions
                     LEFT OUTER JOIN (
                SELECT question_id
                FROM geodl_answers
                WHERE user_id = $1
                GROUP BY question_id
            ) AS answered ON questions.id = answered.question_id
            WHERE answered.question_id IS NULL
            ORDER BY questions.topic_id
            LIMIT 1;
        """, user_id)

    return web.json_response({
        **global_stats,
        'by_topic': [dict(entry) for entry in stats_by_topic],
        'next_suggested_topic_id': next_suggested_topic_id,
    })


def _is_authdata_valid(auth_data):
    message: str = '\n'.join(
        sorted([
            f'{key}={value}' for key, value in auth_data.items() if key != 'hash'
        ])
    )
    key: bytes = hashlib.sha256(TG_BOT_KEY.encode()).digest()

    server_hash = hmac.new(key, message.encode(), hashlib.sha256).hexdigest()
    print(f'{server_hash == auth_data["hash"]}, {server_hash}, {auth_data["hash"]}')
    return server_hash == auth_data['hash']


async def login(req):
    body = await req.json()

    if DEV_ENV:
        async with req.app.pg_pool.acquire() as conn:
            user_id = await conn.fetchval('SELECT id FROM geodl_users WHERE tg_username = $1', body['username'])
    else:
        if not _is_authdata_valid(body):
            return web.HTTPForbidden()

        async with req.app.pg_pool.acquire() as conn:
            user_id = await conn.fetchval('SELECT id FROM geodl_users WHERE tg_user_id = $1', body['id'])

            print('User_id found:', user_id)

            if user_id is None:
                print('New user!', body)
                user_id = await conn.fetchval(
                    '''
                    INSERT INTO geodl_users (tg_user_id, tg_username, tg_first_name, tg_last_name, tg_image_url)
                    VALUES ($1, $2, $3, $4, $5)
                    RETURNING id
                    ''',
                    body['id'], body.get('username'), body.get('first_name'), body.get('last_name'),
                    body.get('photo_url')
                )

    async with req.app.pg_pool.acquire() as conn:
        auth_token = uuid.uuid4().hex
        await conn.execute("INSERT INTO geodl_auth_tokens VALUES($1, $2, now(), now() + INTERVAL '3 days')",
                           auth_token, user_id)

    return web.json_response({
        'user_id': user_id,
        'username': body.get('username'),
        'first_name': body.get('first_name'),
        'token': auth_token
    }, headers={'Set-Cookie': f'auth-token={auth_token}; Path=/; Max-Age={3 * 24 * 60 * 60}; Version=1'})
