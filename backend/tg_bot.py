import os

import aiohttp

TG_BOT_KEY = os.getenv('TG_BOT_KEY')
TG_API_URL = f'https://api.telegram.org/bot{TG_BOT_KEY}'


async def send_message(message: str, user_id: str):
    async with aiohttp.ClientSession() as client:
        res = await client.post(f'{TG_API_URL}/sendMessage', data={'chat_id': user_id, 'text': message})
        print(res.status, await res.text())
        if res.status != 200:
            raise Exception(f'TelegramError: status={res.status} message={await res.text()}')
