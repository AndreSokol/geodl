from aiohttp import web, client
import json

CACHED_REQUESTS = {}


async def handle(req: web.Request):
    data = await req.json()
    data_hash = hash(json.dumps(data))

    if data_hash in CACHED_REQUESTS:
        print('found cached', data_hash)
        return web.json_response(CACHED_REQUESTS[data_hash]['data'],
                                 status=CACHED_REQUESTS[data_hash]['code'])
    print('no cached data, requesting')

    async with client.ClientSession() as c:
        res = await c.post('https://pay.ge/Provider/Balance', data=data)
        response_data = await res.json()
        code = res.status

    CACHED_REQUESTS[data_hash] = {'data': response_data, 'code': code}
    print('cache size:', len(CACHED_REQUESTS))

    return web.json_response(response_data, status=code)
