import asyncio
import json

import requests
import time
import asyncpg

COOKIES = "exam-settings=%7B%22category%22%3A2%2C%22locale%22%3A%22ru%22%2C%22skin%" \
          "22%3A%22dark%22%2C%22user%22%3A0%2C%22created%22%3A1631957083%2C%22quest" \
          "ions%22%3A1000%2C%22challenge%22%3Atrue%2C%22all_questions%22%3Atrue%2C%22" \
          "topics%22%3A%5B%221%22%2C%222%22%2C%223%22%2C%224%22%2C%225%22%2C%226%22%2C" \
          "%227%22%2C%228%22%2C%229%22%2C%2210%22%2C%2211%22%2C%2212%22%2C%2213%22%2C%22" \
          "14%22%2C%2215%22%2C%2216%22%2C%2217%22%2C%2218%22%2C%2219%22%2C%2220%22%2C%2221" \
          "%22%2C%2222%22%2C%2223%22%2C%2224%22%2C%2225%22%2C%2226%22%2C%2227%22%2C%2228%22" \
          "%2C%2229%22%2C%2230%22%2C%2231%22%2C%2232%22%2C%2233%22%5D%2C%22autoShowCorrect%22%3Afalse" \
          "%2C%22autoNextStep%22%3Atrue%7D; Path=/; Expires=Sun, 18 Sep 2023 09:28:38 GMT;"


def main():
    tickets = dict()
    for i in range(1000):
        response = requests.post("http://teoria.on.ge/tickets",
                                 headers={"Cookie": COOKIES, "Content-Type": "application/x-www-form-urlencoded"},
                                 data="cat_id=2&limit=30&log_token=&topics=all&all_questions=false")
        try:
            # print(response.status_code)
            data = response.json()
        except Exception as exc:
            print(exc)
            continue

        for ticket in data:
            try:
                tickets[ticket['id']] = ticket
            except:
                pass

        print(f'Tickets fetched: {len(tickets)}/1083')
        if len(tickets) == 1083:
            break
        time.sleep(0.1)

    with open("out.txt", "w") as f:
        f.write(json.dumps(tickets, ensure_ascii=False, indent=True))


def main2():
    with open("out.txt") as f:
        questions = json.loads(f.read())

    # print(questions)

    questions_prepped = {}
    for i, q in questions.items():
        q["answers"] = {answer['real_id']: answer['text'] for answer in json.loads(q["answers"]).values()}
        questions_prepped[i] = q

    with open("out2.txt", "w") as f:
        f.write(json.dumps(questions_prepped, ensure_ascii=False, indent=True))


async def main3():
    with open("out2.txt") as f:
        questions = json.loads(f.read())

    conn = await asyncpg.connect('postgres://andresokol:andresokol@localhost:5432/postgres')

    for q in questions.values():
        answers = []
        for i in ["1", "2", "3", "4"]:
            if i in q['answers']:
                answers.append(q['answers'][i])

        await conn.execute("""
        INSERT INTO geodl_questions (id, topic_id, question, answers, correct_answer, image)
        VALUES ($1, $2, $3, $4, $5, $6)
        ON CONFLICT DO NOTHING
        """, q['id'], q['topic'], q['question'], answers, q['correct_answer'], q.get('image'))

    await conn.close()


if __name__ == "__main__":
    asyncio.run(main3())
