import asyncio
import json
import os

from aiohttp import web
import asyncpg

from backend import handlers, temp_pay_ge_proxy
from backend.jobs import timeslots_notify, timeslots_fetch


class App(web.Application):
    pg_pool: asyncpg.pool.Pool
    pg_dsn: str
    questions_data: dict

    def __init__(self, pg_dsn=None, **kwargs):
        super().__init__(**kwargs)
        self.pg_dsn = pg_dsn

        with open("out2.txt") as f:
            self.questions_data = json.loads(f.read())

        self.on_startup.append(self._init_pg_pool)
        self.on_shutdown.append(self._close_pg_pool)

        self.on_startup.append(self._start_jobs)

    async def _init_pg_pool(self, *args, **kwargs):
        self.pg_pool = await asyncpg.pool.create_pool(self.pg_dsn)

    async def _close_pg_pool(self, *args, **kwargs):
        await self.pg_pool.close()

    async def _start_jobs(self, *args, **kwargs):
        self._timeslots_fetch_job = asyncio.create_task(timeslots_fetch.timeslots_fetch_job(self))
        self._timeslots_notify_job = asyncio.create_task(timeslots_notify.timeslots_notify_job(self))
        # pass


# @web.middleware
# async def cors_middleware(request, handler):
#     CORS_HEADERS = {
#         'Access-Control-Allow-Headers': '*',
#         'Access-Control-Allow-Methods': 'GET, POST, OPTIONS',
#         'Access-Control-Allow-Origin': '*',
#         'Access-Control-Max-Age': f'{24 * 60 * 60}',  # 24 hours
#     }
#
#     try:
#         response: web.Response = await handler(request)
#         response.headers.update(CORS_HEADERS)
#         return response
#     except web.HTTPException as exc:
#         exc.headers.update(CORS_HEADERS)
#         raise exc


@web.middleware
async def log_request(request: web.Request, handler):
    print(request.method, request.path)
    try:
        response: web.Response = await handler(request)
        print(request.method, request.path, response.status)
        return response
    except web.HTTPException as exc:
        print(request.method, request.path, exc.status)
        raise


def create_app(pg_dsn=None) -> App:
    app = App(
        pg_dsn=pg_dsn,
        middlewares=[
            log_request,
        ],
    )
    # app.router.add_get("/api", handlers.get_next_question)
    app.router.add_post("/api/register_select", handlers.record_response)
    app.router.add_post("/api/login", handlers.login)
    app.router.add_get("/api/user_info", handlers.user_info)
    app.router.add_get('/api/user_stats', handlers.user_stats)

    app.router.add_post('/api/next_question', handlers.handle_next_question)

    app.router.add_post('/api/topic', handlers.fetch_topic)
    # app.add_routes([web.static('/', './frontend/build', show_index=True)])

    # app.router.add_post('/Provider/Balance', temp_pay_ge_proxy.handle)

    return app


if __name__ == "__main__":
    pg_dsn = os.getenv('DATABASE_URL', 'postgres://andresokol:password@localhost:5432/postgres')
    port = os.getenv('PORT', 3001)

    instance = create_app(pg_dsn=pg_dsn)
    web.run_app(app=instance, port=port)
